Из директории корня проекта:

1. Собрать проект: 
`mvn package`
Соберет проект.

2. Запустить сервер: 
`java -jar server/target/server-1.0-SNAPSHOT.jar`
Запустит сервер на порту 8081.

3. Запустить клиент с добавлением значений слова hello: 
`java -jar client\target\client-1.0-SNAPSHOT.jar localhost add hello hi1 hi2 hi3`
Создаст значения hi1, hi2, hi3.

4. Запустить клиент с посмотром значений:
`java -jar client\target\client-1.0-SNAPSHOT.jar localhost get hello`
Вернет значения hi1, hi2, hi3.

5. Запустить клиент с удалением нескольких значений, а также с не существующим значением hi4 
`java -jar client\target\client-1.0-SNAPSHOT.jar localhost delete hello hi2 hi3 hi4`
Удалить только существующие hi2, hi3. Скажет, что значения hi4 не существует.

6. Запустить клиент с посмотром значений:
`java -jar client\target\client-1.0-SNAPSHOT.jar localhost get hello`
Вернет значения hi1.
