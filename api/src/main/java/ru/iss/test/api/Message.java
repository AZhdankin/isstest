package ru.iss.test.api;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Set;

/**
 * Сущьность сообщения, содержащая слово и его значения.
 */
public class Message {
    private String word;
    private Set<String> values;

    public Message() {
    }

    public Message(String word, Set<String> values) {
        this.word = word;
        this.values = values;
    }

    public String getWord() {
        return word;
    }
    public void setWord(String word) {
        this.word = word;
    }

    public Set<String> getValues() {
        return values;
    }
    public void setValues(Set<String> values) {
        this.values = values;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Message message = (Message) o;
        return new EqualsBuilder()
                .append(word, message.word)
                .append(values, message.values)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(word)
                .append(values)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("word", word)
                .append("values", values)
                .toString();
    }
}