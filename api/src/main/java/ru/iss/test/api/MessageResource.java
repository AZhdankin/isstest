package ru.iss.test.api;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * Интерфейс rest-сервиса управления словарем.
 */
@RequestMapping(
        path = MessageResource.URL,
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE
)
public interface MessageResource {
    /**
     * Базовый URL rest-сервиса.
     */
    String URL = "/message";

    /**
     * Добавляет значения слова.
     * @param message Сообщение, содержащее слово и значения для добавления.
     * @throws MessageResourceException Ошибка при добавлении значений слова.
     */
    @PutMapping
    void add(@RequestBody Message message) throws MessageResourceException;


    /**
     * Возвращает значения слова.
     * @param word Слово.
     * @return Сообщение, содержащее слово и все его значения.
     * @throws MessageResourceException Ошибка получения значений слова.
     */
    @GetMapping(value = "/{word}")
    @ResponseBody
    Message get(@PathVariable("word") String word) throws MessageResourceException;

    /**
     * Удаляет значения слова.
     * @param message Сообщение, содержащее слово и значения для удаления.
     * @return Сообщение, содержащее слово и удаленные значения.
     * @throws MessageResourceException Ошибка при удалении.
     */
    @DeleteMapping
    @ResponseBody
    Message delete(@RequestBody Message message) throws MessageResourceException;

    /**
     * Ошибка при взаимодействии со словарем.
     */
    class MessageResourceException extends Exception {
        public MessageResourceException(String message) {
            super(message);
        }
    }
}