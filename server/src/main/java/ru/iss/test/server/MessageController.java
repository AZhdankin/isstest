package ru.iss.test.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.iss.test.api.Message;
import ru.iss.test.api.MessageResource;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

/**
 * Rest-контроллер серверной части усправления словарем.
 */
@RestController
public class MessageController implements MessageResource {

    private static final Logger log = LoggerFactory.getLogger(MessageController.class);

    @Inject
    private MessageService messageService;

    public void add(@RequestBody Message message) throws MessageResourceException {
        log.info("Add message {}", message);
        try {
            messageService.add(message);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            throw new MessageResourceException(ex.getMessage());
        }
    }

    @ResponseBody
    public Message get(@PathVariable("word") String word) throws MessageResourceException {
        log.info("Get word {} request", word);
        try {
            Message values = messageService.get(word);
            log.info("Get word {} result: {}", word, values);
            return values;
        } catch (Exception ex) {
            log.error(ex.getLocalizedMessage(), ex);
            throw new MessageResourceException(ex.getLocalizedMessage());
        }
    }

    @ResponseBody
    public Message delete(@RequestBody Message message) throws MessageResourceException {
        log.info("Delete {}", message);
        try {
            Message deleted = messageService.delete(message);
            log.info("Delete result: {}", message);
            return deleted;
        } catch (Exception ex) {
            log.error(ex.getLocalizedMessage(), ex);
            throw new MessageResourceException(ex.getLocalizedMessage());
        }
    }

    /**
     * Обработчик ошибок для rest-контроллера.
     */
    @RestControllerAdvice
    public static class ExceptionHandlerMessageController {
        @ExceptionHandler(MessageResourceException.class)
        public ResponseEntity<String> defaultErrorHandler(HttpServletRequest req, MessageResourceException ex)
                throws Exception {
            return new ResponseEntity<>(ex.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
