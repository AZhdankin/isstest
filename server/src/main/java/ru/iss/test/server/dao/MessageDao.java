package ru.iss.test.server.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Дао слой управляющий хранением значением слов в Map в конкуретной среде.
 */
@Component
public class MessageDao {
    private static final Logger log = LoggerFactory.getLogger(MessageDao.class);

    private static final Map<String, Set<String>> store = new ConcurrentHashMap<>();

    /**
     * Добавляет значения слова.
     * @param word Слово.
     * @param newValues Значения.
     */
    public void add(String word, final Set<String> newValues) {
        log.trace("word = {}, newValues = {}", word, newValues);

        store.compute(word, (w, currentValues) -> {
            // если нет списка, то создаем новый
            log.trace("currentValues = {}", currentValues);
            if (currentValues == null)
                currentValues = Collections.newSetFromMap(new ConcurrentHashMap<>());

            // добавляем новые значения
            currentValues.addAll(newValues);

            // возвращаем актуальный список для слова
            log.trace("currentValues = {}", currentValues);
            return currentValues;
        });
    }

    /**
     * Возвращает значения слова.
     * @param word Слово.
     * @return Значения слова.
     */
    public Set<String> get(String word) {
        log.trace("word = {}", word);

        // получаем список
        Set<String> set = store.get(word);
        log.trace("set = {}", set);

        // возвращаем другим списком
        HashSet<String> result = new HashSet<>();
        if (set != null) {
            result.addAll(set);
        }
        log.trace("result = {}", result);

        return result;
    }

    /**
     * Удаляет значения слова.
     * @param word Слово.
     * @param deletingValues Удаляемые значения.
     * @return Удаленные значения.
     */
    public Set<String> delete(String word, final Set<String> deletingValues) {
        log.trace("word = {}, deletingValues = {}", word, deletingValues);
        final Set<String> deletedValues = new HashSet<>(deletingValues.size());

        store.compute(word, (w, currentValues) -> {
            log.trace("currentValues = {}", currentValues);

            // если нет списка значений, сразу выходим
            if (currentValues == null)
                return null;

            // удаляем значения из списка значений
            for (String deletingValue : deletingValues) {
                if (currentValues.remove(deletingValue)) {
                    // если удалили, то записываем значение в список удаленных
                    deletedValues.add(deletingValue);
                }
            }
            log.trace("deletingValues = {}, currentValues = {}", deletingValues, currentValues);

            // если новый список получился пустой, то удаляем слово, вернув null
            if (currentValues.isEmpty())
                return null;
            else
                return currentValues;
        });

        // возвращаем список удаленных значений
        return deletedValues;
    }
}
