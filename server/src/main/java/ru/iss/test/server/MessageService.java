package ru.iss.test.server;

import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.iss.test.api.Message;
import ru.iss.test.server.dao.MessageDao;

import javax.inject.Inject;
import java.util.Set;

/**
 * Сервис с логикой управления словарем.
 */
@Service
public class MessageService {
    private static final Logger log = LoggerFactory.getLogger(MessageService.class);

    @Inject
    private MessageDao messageDao;

    /**
     * Добавлет значения слова.
     * @param message Сообщение со словом и новыми значениями.
     */
    public void add(Message message) {
        log.debug("message = {}", message);

        String word = message.getWord();
        Validate.notBlank(word, "Word cannot be null");

        Set<String> values = message.getValues();
        Validate.notEmpty(values, "Values cannot be empty");

        messageDao.add(word, values);
    }

    /**
     * Возвращает значения слова.
     * @param word Слово.
     * @return Сообщение со словом и значениями.
     */
    public Message get(String word) {
        log.debug("word = {}", word);

        Validate.notBlank(word, "Word cannot be null");
        Set<String> values = messageDao.get(word);

        Message message = new Message(word, values);
        log.debug("message = {}", message);
        return message;
    }

    /**
     * Удаляет значения слова.
     * @param requestMessage Сообщение со словом и значениями для удаления.
     * @return Сообщение со словом и удаленными значениями.
     */
    public Message delete(Message requestMessage) {
        log.debug("message = {}", requestMessage);

        String word = requestMessage.getWord();
        Validate.notBlank(word, "Word cannot be null");

        Set<String> values = requestMessage.getValues();
        Validate.notEmpty(values, "Values cannot be empty");

        Set<String> deletedValues = messageDao.delete(word, values);
        Message responseMessage = new Message(word, deletedValues);
        log.debug("responseMessage = {}", responseMessage);
        return responseMessage;
    }
}
