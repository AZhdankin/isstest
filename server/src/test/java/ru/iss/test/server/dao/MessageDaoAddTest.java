package ru.iss.test.server.dao;

import org.junit.Test;
import java.util.Set;

import static org.junit.Assert.*;
import static ru.iss.test.server.TestUtils.asSet;

public class MessageDaoAddTest {

    private MessageDao dao = new MessageDao();

    @Test
    public void test() {
        // первичное добавление значений слова
        dao.add("hello", asSet("hi1", "hi2", "hi3"));

        { // проверка получения значений
            Set<String> values = dao.get("hello");
            assertTrue(values.contains("hi1"));
            assertTrue(values.contains("hi2"));
            assertTrue(values.contains("hi3"));
        }

        { // удаление одного значения
            Set<String> deleted = dao.delete("hello", asSet("hi3", "hi4", "hi5"));
            assertTrue(!deleted.contains("hi1"));
            assertTrue(!deleted.contains("hi2"));
            assertTrue(deleted.contains("hi3"));
            assertTrue(!deleted.contains("hi4"));
            assertTrue(!deleted.contains("hi5"));
        }

        { // проверка значений после удаления
            Set<String> values = dao.get("hello");
            assertTrue(values.contains("hi1"));
            assertTrue(values.contains("hi2"));
            assertTrue(!values.contains("hi3"));
        }
    }
}