package ru.iss.test.server;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class TestUtils {

    public static Set<String> asSet(String... values) {
        return new HashSet<>(Arrays.asList(values));
    }
}