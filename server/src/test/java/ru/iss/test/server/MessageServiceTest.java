package ru.iss.test.server;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ru.iss.test.api.Message;
import ru.iss.test.server.dao.MessageDao;

import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static ru.iss.test.server.TestUtils.asSet;

@RunWith(MockitoJUnitRunner.class)
public class MessageServiceTest {

    private static final String WORD = "hello";
    private static final Set<String> VALUES = asSet("hi1", "hi2", "hi3");

    @Mock
    private MessageDao messageDao;

    @InjectMocks
    private MessageService messageService;

    @Test
    public void testAddSuccess() {
        Message message = new Message(WORD, VALUES);
        messageService.add(message);

        verify(messageDao).add(WORD, VALUES);
    }

    @Test(expected = Exception.class)
    public void testAddExceptionEmptyWord() {
        Message message = new Message("", VALUES);
        messageService.add(message);
    }

    @Test(expected = Exception.class)
    public void testAddExceptionNullWord() {
        Message message = new Message(null, VALUES);
        messageService.add(message);
    }

    @Test(expected = Exception.class)
    public void testAddExceptionEmptyValues() {
        Message message = new Message(WORD, null);
        messageService.add(message);
    }

    @Test(expected = Exception.class)
    public void testAddExceptionNullValues() {
        Message message = new Message(WORD, asSet((String[]) null));
        messageService.add(message);
    }

    @Test
    public void testGetSuccessNotAddedWord() {
        Message message = messageService.get(WORD);
        assertNotNull(message);

        String word = message.getWord();
        assertEquals(WORD, word);

        Set<String> values = message.getValues();
        assertNotNull(values);
        assertTrue(values.isEmpty());
    }

    @Test
    public void testGetSuccessAddedWord() {
        when(messageDao.get(eq(WORD))).thenReturn(VALUES);

        Message message = messageService.get(WORD);
        assertNotNull(message);

        String word = message.getWord();
        assertEquals(WORD, word);

        Set<String> values = message.getValues();
        assertNotNull(values);
        assertEquals(VALUES, values);
    }

    @Test(expected = Exception.class)
    public void testGetExceptionEmptyWord() {
        messageService.get("");
    }

    @Test(expected = Exception.class)
    public void testGetExceptionNullWord() {
        messageService.get(null);
    }

    @Test
    public void testDeleteSuccess() {
        when(messageDao.delete(eq(WORD), eq(VALUES))).thenReturn(VALUES);

        Message message = new Message(WORD, VALUES);
        Message deleted = messageService.delete(message);

        assertNotNull(deleted);
        assertEquals(WORD, deleted.getWord());
        assertNotNull(deleted.getValues());
        assertEquals(VALUES, deleted.getValues());

        verify(messageDao).delete(WORD, VALUES);
    }

    @Test(expected = Exception.class)
    public void testDeleteExceptionEmptyWord() {
        Message message = new Message("", VALUES);
        messageService.delete(message);
    }

    @Test(expected = Exception.class)
    public void testDeleteExceptionNullWord() {
        Message message = new Message(null, VALUES);
        messageService.delete(message);
    }

    @Test(expected = Exception.class)
    public void testDeleteExceptionEmptyValues() {
        Message message = new Message(WORD, asSet((String[]) null));
        messageService.delete(message);
    }

    @Test(expected = Exception.class)
    public void testDeleteExceptionNullValues() {
        Message message = new Message(WORD, null);
        messageService.delete(message);
    }
}