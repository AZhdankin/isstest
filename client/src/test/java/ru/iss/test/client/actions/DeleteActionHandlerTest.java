package ru.iss.test.client.actions;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ru.iss.test.api.Message;
import ru.iss.test.client.ConsoleWriter;
import ru.iss.test.client.client.MessageResourceClient;

import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;
import static ru.iss.test.client.actions.TestUtils.asSet;

@RunWith(MockitoJUnitRunner.class)
public class DeleteActionHandlerTest {

    @Mock
    private MessageResourceClient client;

    @Mock
    private ConsoleWriter console;

    @Captor
    private ArgumentCaptor<String> consoleCaptor;

    @Captor
    private ArgumentCaptor<Message> messageCaptor;

    @InjectMocks
    private DeleteActionHandler handler;

    @Test
    public void test_doAction_successNoOneWordExists() throws Exception {
        // ответ клиента на удаление значений слова
        when(client.delete(eq(new Message("hello", asSet("hi1", "hi2", "hi3")))))
                .thenReturn(new Message("hello", new HashSet<>())); // ни одно слово не удалено

        doAction();

        // проверка вывода пользователю
        verify(console, atLeast(0)).write(consoleCaptor.capture());
        verify(console, atLeast(0)).write(anyString(), consoleCaptor.capture());
        List<String> allWrites = consoleCaptor.getAllValues();
        List<String> lookingFor = Arrays.asList("hello",
                "<слово {} или его значения {} отсутствуют в словаре>",
                "hi1", "hi2", "hi3");
        for (Object obj : allWrites) {
            if (obj instanceof Collection) {
                for (Object obj2 : (Collection) obj) {
                    assertTrue(lookingFor.contains(obj2));
                }
            } else {
                assertTrue(lookingFor.contains(obj));
            }
        }
    }

    @Test
    public void test_doAction_successHalfWordExists() throws Exception {
        // ответ клиента на удаление значений слова
        when(client.delete(eq(new Message("hello", asSet("hi1", "hi2", "hi3")))))
                .thenReturn(new Message("hello", asSet("hi3"))); // удалено только hi3

        doAction();

        // проверка вывода пользователю
        verify(console, atLeast(0)).write(consoleCaptor.capture());
        verify(console, atLeast(0)).write(anyString(), consoleCaptor.capture());
        List<String> allWrites = consoleCaptor.getAllValues();
        List<String> lookingFor = Arrays.asList("hello",
                "<значения слова {}: {} отсутствуют в словаре>",
                "<остальные значения слова успешно удалены>",
                "hi1", "hi2");
        for (Object obj : allWrites) {
            if (obj instanceof Collection) {
                for (Object obj2 : (Collection) obj) {
                    assertTrue(lookingFor.contains(obj2));
                }
            } else {
                assertTrue(lookingFor.contains(obj));
            }
        }
    }

    @Test
    public void test_doAction_successAllWordExists() throws Exception {
        // ответ клиента на удаление значений слова
        when(client.delete(eq(new Message("hello", asSet("hi1", "hi2", "hi3")))))
                .thenReturn(new Message("hello", asSet("hi1", "hi2", "hi3")));

        doAction();

        // проверка вывода пользователю
        verify(console).write(consoleCaptor.capture());
        assertEquals("<значения слова успешно удалены>", consoleCaptor.getValue());
    }

    private void doAction() throws Exception {
        handler.doAction("hello hi1 hi2 hi3".split(" "));

        // проверка корректности передачи в клиент
        verify(client).delete(messageCaptor.capture());
        Message message = messageCaptor.getValue();
        assertNotNull(message);
        String word = message.getWord();
        assertEquals("hello", word);
        Set<String> values = message.getValues();
        assertNotNull(values);
        assertTrue(values.contains("hi1"));
        assertTrue(values.contains("hi2"));
        assertTrue(values.contains("hi3"));
    }

    @Test(expected = Exception.class)
    public void test_doAction_exceptionArgsIsNull() throws Exception {
        handler.doAction(new String[]{null});
    }

    @Test(expected = Exception.class)
    public void test_doAction_exceptionWordIsNull() throws Exception {
        handler.doAction(" hi1 hi2 hi3".split(" "));
    }

    @Test(expected = Exception.class)
    public void test_doAction_exceptionValuesIsNull() throws Exception {
        handler.doAction("hello ".split(" "));
    }
}