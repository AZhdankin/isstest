package ru.iss.test.client.actions;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.verification.VerificationMode;
import ru.iss.test.api.Message;
import ru.iss.test.client.ConsoleWriter;
import ru.iss.test.client.client.MessageResourceClient;

import javax.inject.Inject;

import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static ru.iss.test.client.actions.TestUtils.asSet;

@RunWith(MockitoJUnitRunner.class)
public class GetActionHandlerTest {

    @Mock
    private MessageResourceClient client;

    @Mock
    private ConsoleWriter console;

    @Captor
    private ArgumentCaptor<String> consoleCaptor;

    @Captor
    private ArgumentCaptor<String> wordCaptor;

    @InjectMocks
    private GetActionHandler handler;

    @Test
    public void test_doAction_successWordExists() throws Exception {
        Set<String> values = asSet("hi1", "hi2", "hi3");
        when(client.get(eq("hello"))).thenReturn(new Message("hello", values));

        handler.doAction("hello".split(" "));
        verify(client).get(wordCaptor.capture());
        verify(console, times(values.size())).write(consoleCaptor.capture());

        String word = wordCaptor.getValue();
        assertEquals("hello", word);

        List<String> allWrites = consoleCaptor.getAllValues();
        for (String value : values) {
            assertTrue(allWrites.contains(value));
        }
    }

    @Test
    public void test_doAction_successWordNotExists() throws Exception {
        when(client.get(eq("hello"))).thenReturn(new Message("hello", null));

        handler.doAction("hello".split(" "));
        verify(client).get(wordCaptor.capture());
        verify(console).write(consoleCaptor.capture());

        String word = wordCaptor.getValue();
        assertEquals("hello", word);
        assertEquals("<слово отсутвует в словаре>", consoleCaptor.getValue());
    }

    @Test(expected = Exception.class)
    public void test_doAction_exceptionWordIsNull() throws Exception {
        handler.doAction(new String[]{null});
    }
}