package ru.iss.test.client;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.BeanFactory;
import ru.iss.test.client.actions.ActionHandler;
import ru.iss.test.client.actions.AddActionHandler;
import ru.iss.test.client.actions.GetActionHandler;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ServiceFactoryTest {

    @Mock
    private BeanFactory beanFactory;

    @InjectMocks
    private ServiceFactory factory;

    @Test
    public void test_getActionServiceByCommand_add() {
        when(beanFactory.getBean("add", ActionHandler.class)).thenReturn(new AddActionHandler());
        ActionHandler handler = factory.getActionHandlerByCommand("add");
        assertTrue(handler instanceof AddActionHandler);
    }
}