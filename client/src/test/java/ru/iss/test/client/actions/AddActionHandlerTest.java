package ru.iss.test.client.actions;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ru.iss.test.api.Message;
import ru.iss.test.client.ConsoleWriter;
import ru.iss.test.client.client.MessageResourceClient;

import javax.inject.Inject;

import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class AddActionHandlerTest {

    @Mock
    private MessageResourceClient client;

    @Mock
    private ConsoleWriter console;

    @Captor
    private ArgumentCaptor<Message> messageCaptor;

    @Captor
    private ArgumentCaptor<String> consoleCaptor;

    @InjectMocks
    private AddActionHandler handler;

    @Test
    public void test_DoAction_Success() throws Exception {
        handler.doAction("hello hi1 hi2 hi3".split(" "));

        verify(client).add(messageCaptor.capture());
        verify(console).write(consoleCaptor.capture());

        Message message = messageCaptor.getValue();
        assertNotNull(message);
        assertTrue(StringUtils.isNotBlank(message.getWord()));
        assertNotNull(message.getValues());
        assertEquals(3, message.getValues().size());
        assertEquals("<значения слова успешно добавлены>", consoleCaptor.getValue());
    }

    @Test(expected = Exception.class)
    public void test_DoAction_ExceptionEmptyWord() throws Exception {
        handler.doAction(" hi1 hi2 hi3".split(" "));
    }

    @Test(expected = Exception.class)
    public void test_DoAction_ExceptionEmptyValues() throws Exception {
        handler.doAction("hello   ".split(" "));
    }

    @Test(expected = Exception.class)
    public void test_DoAction_ExceptionNullValues() throws Exception {
        handler.doAction("hello".split(" "));
    }
}