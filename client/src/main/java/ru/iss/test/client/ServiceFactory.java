package ru.iss.test.client;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.stereotype.Component;
import ru.iss.test.client.actions.ActionHandler;

import javax.inject.Inject;

/**
 * Фабрика для динамеческого получения бинов сервисов.
 */
@Component
public class ServiceFactory {

    @Inject
    private BeanFactory beanFactory;

    /**
     * Возвращает обработчик команды по ее имени.
     * @param command Имя команды
     * @return Инстанс обработчика команды.
     */
    public ActionHandler getActionHandlerByCommand(String command) {
        return beanFactory.getBean(command.toLowerCase(), ActionHandler.class);
    }
}
