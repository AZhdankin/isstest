package ru.iss.test.client.actions;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import ru.iss.test.api.Message;
import ru.iss.test.client.ConsoleWriter;
import ru.iss.test.client.client.MessageResourceClient;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Set;

/**
 * Обработчик команды на получение значений слов.
 */
@Service
@Named(ActionHandler.GET)
public class GetActionHandler implements ActionHandler {

    @Inject
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    private MessageResourceClient client;

    @Inject
    private ConsoleWriter console;

    @Override
    public void doAction(String[] args) throws Exception {
        // валидация входных параметров
        Validate.isTrue(ArrayUtils.isNotEmpty(args), "Required Get-command's argument 'word' not found");
        String word = args[0];
        Validate.notBlank(word, "Word cannot be empty");

        // запрос слова
        Message message = client.get(word);
        Set<String> values = message.getValues();
        if (CollectionUtils.isEmpty(values)) {
            console.write("<слово отсутвует в словаре>");
        } else {
            for (String value : values) {
                console.write(value);
            }
        }
    }
}