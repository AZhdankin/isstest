package ru.iss.test.client.actions;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.iss.test.api.Message;
import ru.iss.test.client.ConsoleWriter;
import ru.iss.test.client.client.MessageResourceClient;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

/**
 * Обработчик команды на удаление значений слов.
 */
@Service
@Named(ActionHandler.DELETE)
public class DeleteActionHandler implements ActionHandler {

    @Inject
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    private MessageResourceClient client;

    @Inject
    private ConsoleWriter console;

    @Override
    public void doAction(String[] args) throws Exception {
        // получение слова и значений
        Validate.isTrue(ArrayUtils.isNotEmpty(args), "Required Add-command's argument 'word' not found");
        String word = args[0];
        String[] values = ArrayUtils.remove(args, 0);
        Validate.isTrue(ArrayUtils.isNotEmpty(values), "Required Add-command's argument 'values' not found");

        // составление и отправка сообщения
        HashSet<String> deletingValues = new HashSet<>(Arrays.asList(values));
        Message requestMessage = new Message(word, deletingValues);
        Message responseMessage = client.delete(requestMessage);

        // сравнение запрошенных на удвление значение с удаленными значениями
        Collection<String> deletedValues = responseMessage.getValues();
        Collection<String> failDeletedValues = CollectionUtils.disjunction(deletingValues, deletedValues);

        if (failDeletedValues.isEmpty()) {
            // если список значений удаления в ошибкой пуст, значит они все удалены
            console.write("<значения слова успешно удалены>");
        } else if (deletingValues.size() != failDeletedValues.size()) {
            // если только часть значений была удалена
            console.write("<значения слова {}: {} отсутствуют в словаре>", word, failDeletedValues);
            console.write("<остальные значения слова успешно удалены>");
        } else {
            // если ни одно значение не было удалено.
            console.write("<слово {} или его значения {} отсутствуют в словаре>", word, failDeletedValues);
        }
    }
}