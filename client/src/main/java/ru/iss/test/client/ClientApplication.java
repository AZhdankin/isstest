package ru.iss.test.client;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import ru.iss.test.client.actions.ActionHandler;
import ru.iss.test.client.actions.GetActionHandler;

import javax.inject.Inject;

/**
 * Главный класс клиента.
 */
@SpringBootApplication
@ComponentScan("ru.iss.test.client")
@EnableFeignClients
public class ClientApplication implements CommandLineRunner {
    private static final Logger log = LoggerFactory.getLogger(GetActionHandler.class);

    @Inject
    private ServiceFactory serviceFactory;

    static {
        // установка короткого формата для toString-методов сущьностей
        ToStringBuilder.setDefaultStyle(ToStringStyle.SHORT_PREFIX_STYLE);
    }

    /**
     * Запускает клиент, выполняет первичную валидацию команды и обрабатывает настройки окружения клиента.
     * @param args Параметры клиента с командой.
     */
    public static void main(String[] args) {
        log.debug("args: {}", args);

        try {
            // получение адреса сервера из параметров
            Validate.isTrue(ArrayUtils.isNotEmpty(args), "Required server argument not found");
            String serverHost = args[0];
            System.setProperty("server.host", serverHost);
            log.info("Server host: {}", serverHost);
            args = ArrayUtils.remove(args, 0);

            // запуск приложения
            SpringApplication.run(ClientApplication.class, args);
        } catch (Exception ex) {
            log.info("Error: <{}>", ex.getLocalizedMessage());
            log.error(ex.getLocalizedMessage(), ex);
        }
    }

    /**
     * Выполняет валидацию, выбирает обработчик команды и передает ему управление.
     * @param args Команда клиента.
     * @throws Exception Ошибка выполнения операции.
     */
    public void run(String... args) throws Exception {
        // проверка наличия команды
        Validate.isTrue(ArrayUtils.isNotEmpty(args), "Required command argumaent not found");

        // получение команды и аргументов
        String command = args[0];
        args = ArrayUtils.remove(args, 0);

        // вызов обработчика команды
        ActionHandler actionHandler = serviceFactory.getActionHandlerByCommand(command);
        actionHandler.doAction(args);
    }
}
