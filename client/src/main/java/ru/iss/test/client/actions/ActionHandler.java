package ru.iss.test.client.actions;

/**
 * Интерфейс обработчика команды.
 */
public interface ActionHandler {
    String ADD = "add";
    String GET = "get";
    String DELETE = "delete";

    void doAction(String[] args) throws Exception;
}