package ru.iss.test.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Выводит пользователю ответ в консоль.
 * (Использует логгер, см. настройки логирования.)
 */
@Component
public class ConsoleWriter {

    private static final Logger log = LoggerFactory.getLogger(ConsoleWriter.class);

    public void write(String string) {
        log.info(string);
    }

    public void write(String template, Object... args) {
        log.info(template, args);
    }
}
