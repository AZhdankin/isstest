package ru.iss.test.client.actions;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.iss.test.api.Message;
import ru.iss.test.client.ConsoleWriter;
import ru.iss.test.client.client.MessageResourceClient;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Arrays;
import java.util.HashSet;

/**
 * Обработчик команды на добавление значений слов.
 */
@Service
@Named(ActionHandler.ADD)
public class AddActionHandler implements ActionHandler {

    @Inject
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    private MessageResourceClient client;

    @Inject
    private ConsoleWriter console;

    public void doAction(String[] args) throws Exception {
        // получение слова и значений
        Validate.isTrue(ArrayUtils.isNotEmpty(args),
                "Required Add-command's argument 'word' not found");
        String word = args[0];
        Validate.notBlank(word, "Word cannot be empty or null");

        String[] values = ArrayUtils.remove(args, 0);
        Validate.isTrue(ArrayUtils.isNotEmpty(values),
                "Required Add-command's argument 'values' not found");

        // составление и отправка сообщения
        Message message = new Message(word, new HashSet<>(Arrays.asList(values)));
        client.add(message);
        console.write("<значения слова успешно добавлены>");
    }
}