package ru.iss.test.client.client;

import org.springframework.cloud.openfeign.FeignClient;
import ru.iss.test.api.MessageResource;

/**
 * Клиент для rest-сервиса словаря.
 */
@FeignClient(name="client", url = "http://${server.host}:${server.port}")
public interface MessageResourceClient extends MessageResource {
}